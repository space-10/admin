<?php
namespace Space10\Admin;

use Zend\ModuleManager\Feature\BootstrapListenerInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\DependencyIndicatorInterface;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;

/**
 * Class Module
 * @package Space10\Admin
 */
class Module implements ConfigProviderInterface, BootstrapListenerInterface
{

    /**
     * {@inheritDoc}
     */
    public function onBootstrap(\Zend\EventManager\EventInterface $event)
    {
        $app = $event->getParam('application');
        $eventManager = $app->getEventManager();
        $eventManager->attach(MvcEvent::EVENT_DISPATCH, [
            $this,
            'selectLayout',
        ]);
    }

    /**
     * @param MvcEvent $event
     */
    public function selectLayout(MvcEvent $event)
    {
        /* @var $app \Zend\Mvc\Application */
        $app = $event->getParam('application');
        $sm = $app->getServiceManager();
        $match = $event->getRouteMatch();
        $config = $sm->get('config');
        if (! $match instanceof RouteMatch || strpos($match->getMatchedRouteName(), $config['space10admin']['route_name']) !== 0) {
            return;
        }

        $layout = isset($config['space10admin']['layout']) ? $config['space10admin']['layout'] : 'layout/admin';
        $view = $event->getViewModel();
        $view->setTemplate($layout);
    }

    /*
     * (non-PHPdoc) @see \Zend\ModuleManager\Feature\ConfigProviderInterface::getConfig()
     */
    public function getConfig()
    {
        return include __DIR__ . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'module.config.php';
    }
}
