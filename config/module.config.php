<?php
namespace Space10\Admin;

return [
    'space10admin'    => [
        'layout'       => 'layout/admin',
        'route_name'   => 'admin',
        'config_paths' => [
            'config/admin/menu.xml',
        ],
    ],
    'controllers'     => [
        'invokables' => [
            'Space10\Admin\Controller\Dashboard' => 'Space10\Admin\Controller\DashboardController',
        ],
    ],
    'service_manager' => [
        'factories'  => [
            'Space10\Admin\Navigation' => 'Space10\Admin\Navigation\MenuFactory',
        ],
        'invokables' => [
            'Space10\Admin\Navigation\ConfigReader' => 'Space10\Admin\Navigation\ConfigReader',
        ],
    ],
    'router'          => [
        'routes' => [
            'admin' => [
                'type'          => 'Literal',
                'options'       => [
                    'route'    => '/admin',
                    'defaults' => [
                        'controller' => 'Space10\Admin\Controller\Dashboard',
                        'action'     => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes'  => [
                    'dashboard' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'    => '/dashboard',
                            'defaults' => [
                                'controller' => 'Space10\Admin\Controller\Dashboard',
                                'action'     => 'index',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    'view_manager'    => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
    ],
    'translator'      => [
        'locale'                    => 'en_US',
        'translation_file_patterns' => [
            [
                'type'        => 'gettext',
                'base_dir'    => __DIR__ . '/../resources/language',
                'pattern'     => '%s.mo',
                'text_domain' => __NAMESPACE__,
            ],
        ],
    ],
];
