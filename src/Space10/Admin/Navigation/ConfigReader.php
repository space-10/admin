<?php
/**
 * Created by PhpStorm.
 * User: Tobias Trozowski
 * Date: 02.03.2015
 * Time: 20:46
 */

namespace Space10\Admin\Navigation;

use Magento\Framework\Simplexml\Config;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ConfigReader implements ServiceLocatorAwareInterface
{

    /**
     * @var ServiceLocatorInterface
     */
    protected $serviceLocator;

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var Config
     */
    protected static $prototype;

    protected static function getPrototype()
    {
        if (!self::$prototype) {
            self::$prototype = new Config();
        }
        return self::$prototype;
    }

    /**
     * Set service locator
     *
     * @param ServiceLocatorInterface $serviceLocator
     *
     * @return $this
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        return $this;
    }

    /**
     * Get service locator
     *
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    /**
     * @return Config
     */
    public function getConfig()
    {
        if (!$this->config) {
            $mergeToObject = clone static::getPrototype();
            $mergeToObject->loadString('<config/>');
            $mergeModel = clone static::getPrototype();

            $files = $this->getConfigPaths($this->serviceLocator);
            foreach ($files as $configFile) {
                if ($mergeModel->loadFile($configFile)) {
                    $mergeToObject->extend($mergeModel, true);
                }
            }
            $this->config = $mergeToObject;
        }
        return $this->config;
    }

    /**
     * @param ServiceLocatorInterface $serviceLocator
     *
     * @return array
     */
    protected function getConfigPaths(ServiceLocatorInterface $serviceLocator)
    {
        $configFiles = [];
        $moduleConfig = $serviceLocator->get('config');
        $configFilePaths = $moduleConfig['space10admin']['config_paths'];
        $moduleManager = $serviceLocator->get('ModuleManager');
        $modules = $moduleManager->getLoadedModules();
        foreach ($modules as $moduleName => $module) {
            $moduleClass = new \ReflectionClass($module);
            $modulePath = dirname($moduleClass->getFileName());
            foreach ($configFilePaths as $filePath) {
                $configFile = $modulePath . DIRECTORY_SEPARATOR . $filePath;
                if (file_exists($configFile)) {
                    $configFiles[] = $configFile;
                }
            }
        }
        return $configFiles;
    }
}
