<?php
namespace Space10\Admin\Navigation\Exception;

class MissingElementException extends \InvalidArgumentException implements ExceptionInterface
{
}
