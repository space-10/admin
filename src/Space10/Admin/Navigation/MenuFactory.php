<?php
namespace Space10\Admin\Navigation;

use Magento\Framework\Simplexml\Element;
use Zend\Mvc\Router\RouteMatch;
use Zend\Mvc\Router\RouteStackInterface;
use Zend\Navigation\Service\DefaultNavigationFactory;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Validator\Uri;

/**
 * Factory for the admin navigation
 */
class MenuFactory extends DefaultNavigationFactory
{

    protected $config;

    /**
     * @{inheritdoc}
     */
    protected function getName()
    {
        return 'Admin_Navigation';
    }

    protected function getPages(ServiceLocatorInterface $serviceLocator)
    {
        if (null === $this->pages) {

            /** @var \Space10\Admin\Navigation\ConfigReader $configReader */
            $configReader = $serviceLocator->get('Space10\Admin\Navigation\ConfigReader');
            $config = $configReader->getConfig();

            $menuItems = $config->getNode('menu')->children();

            $application = $serviceLocator->get('Application');
            $routeMatch = $application->getMvcEvent()->getRouteMatch();
            $router = $application->getMvcEvent()->getRouter();
            $this->pages = $this->injectMenuComponents($menuItems, $routeMatch, $router);
        }
        return $this->pages;
    }

    /**
     *
     * @link     http://framework.zend.com/manual/current/en/modules/zend.navigation.pages.html
     *
     * @param Element                    $menuItems
     * @param RouteMatch                 $routeMatch
     * @param Router|RouteStackInterface $router
     *
     * @return mixed
     * @internal param array $pages
     * @todo     add support for zend route params
     */
    protected function injectMenuComponents(
        Element $menuItems,
        RouteMatch $routeMatch = null,
        RouteStackInterface $router = null
    ) {
        $pages = [];
        foreach ($menuItems as $item) {
            $page = [];
            $page['type'] = 'mvc';
            /* @var $item \Magento\Framework\Simplexml\Element */

            $action = (string)$item->action;
            if (empty($action)) {
                $action = '#';
            }

            $label = (string)$item->label;
            if (empty($label)) {
                throw new Exception\MissingElementException('Missing required element "label" for node "' . $item->getName() . '".');
            }

            $order = $item->sort_order && !empty($item->sort_order) ? (string)$item->sort_order : 0;

            $page['label'] = $label;
            $page['order'] = (int)$order;

            $validator = new Uri(['allowRelative' => false]);
            if ($action === '#' || $validator->isValid($action)) {
                $page['type'] = 'uri';
                $page['uri'] = '';
            } else {
                // @fixme check if action is a defined route in zend router
                $page['route'] = $action;
                if (!isset($page['routeMatch']) && $routeMatch) {
                    $page['routeMatch'] = $routeMatch;
                }
                if (!isset($page['router'])) {
                    $page['router'] = $router;
                }
            }

            // access xml node "children"
            $childEl = $item->children;
            if ($childEl && $childEl->count() > 0) {
                $page['pages'] = $this->injectMenuComponents($childEl->children(), $routeMatch, $router);
            }
            $pages[] = $page;
        }
        return $pages;
    }
}
