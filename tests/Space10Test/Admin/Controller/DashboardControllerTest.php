<?php

namespace Space10Test\Admin\Controller;

use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

class DashboardControllerTest extends AbstractHttpControllerTestCase
{

    public function setUp()
    {
        $this->setApplicationConfig(
            require __DIR__ . '/../../../test.config.php'
        );
        parent::setUp();
    }

    public function testAdminDashboardCanBeAccessed()
    {
        $this->dispatch('/admin');
        $this->assertResponseStatusCode(200);

        $this->assertModuleName('space10');
        $this->assertControllerName('space10\admin\controller\dashboard');
        $this->assertControllerClass('DashboardController');
        $this->assertMatchedRouteName('admin');
    }
}
