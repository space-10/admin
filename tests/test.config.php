<?php
return [
    'modules' => [
        'Space10\Admin', // <- my 'Rest' module is the one I test here.
    ],
    'module_listener_options' => [
        'module_paths' => [
            'module',
            'vendor',
        ],
    ],
];
